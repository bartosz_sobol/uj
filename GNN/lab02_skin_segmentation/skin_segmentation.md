---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.0
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Skin segmentation

+++

In this assignement you will train classifier to assign colors to skin or no skin classes. The data is taken from [Skin Segmentation Data Set](http://archive.ics.uci.edu/ml/datasets/Skin+Segmentation#) in the UCI Machine Learning repository.

+++

The  data is in a plain text format and contains four columns. First three contain RGB color data  represented as integers in the range 0-255, and the last column is an integer label  with 1 representing skin and 2 representing no skin. This file we can load directly into a numpy array:

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt
import torch
from torch import nn
from torch.utils.data import DataLoader
```

```{code-cell} ipython3
data = np.loadtxt('data/Skin_NonSkin.txt')
```

```{code-cell} ipython3
rgb  = data[:,:3].astype('float32')
lbl = data[:,3].astype('float32') 
lbl = 2-lbl
```

```{code-cell} ipython3
len(data)
```

```{code-cell} ipython3
np.bincount(lbl.astype('int32'))
```

## Problem 1

```{code-cell} ipython3

```

Train the neural network to distinguish skin from no skin colors. Calculate the accuracy on train and validation sets. Calculate true positives rate and false positives rate.

```{code-cell} ipython3
class NN01(nn.Module):
    def __init__(self, hidden_size):
        super(NN01, self).__init__()
        
        self.hidden_size = hidden_size
        
        self.stack = nn.Sequential(
            nn.Linear(3, hidden_size),
            nn.Sigmoid(),
            nn.Linear(hidden_size, 1)            
        )

    def forward(self, x):
        return self.stack(x)
```

```{code-cell} ipython3
class Runner01:
    def __init__(self, split = 0.8, batch_size=64, lr=0.01, verbose=True):
        self.batch_size = batch_size
        self.lr = lr
        self.split = split
        self.verbose = verbose
        
        self.model = NN01()
        self.loss = nn.MSELoss()
        self.optim = torch.optim.SGD(self.model.parameters(), lr=self.lr)
        
        train_data, test_data = Runner01.load_data(self.split)        
        self.train_loader = DataLoader(train_data, 
                                       batch_size=self.batch_size, shuffle=True)
        self.test_loader = DataLoader(test_data, 
                                      batch_size=self.batch_size, shuffle=True)

    def train(self, n_epochs=3):
        print(f'training...', flush=True)
        for i_epoch in range(n_epochs):
            print(f'\tepoch {i_epoch}...', flush=True)
            self.model.train()
            for i, (data, labels) in enumerate(self.train_loader):                
                self.optim.zero_grad()
                output = self.model(data)
                loss = self.loss(output, labels)
                loss.backward()
                self.optim.step()

                if (i + 1) % 100 == 0:
                    print(f'Epoch {i_epoch} iter {i + 1}/{len(self.train_data) // self.batch_size} loss: {loss.item()}')
        
            self.model.eval()
            # ......

    def test(self):
        pass
    
    @staticmethod
    def load_data():
        print(f'loading data...', flush=True)
        data = np.loadtxt('data/Skin_NonSkin.txt')
        rgb  = data[:,:3].astype('float32')
        labels = 2 - data[:,3].astype('float32') 
        print('done')
        
        torch_data = torch.utils.data.TensorDataset()
        train_size = int(split * len(data))
        test_size = len(data) - train_size
        train_data, test_data = torch.utils.data.random_split(torch_data, (train_size, test_size))
        
        return train_data, test_data
       
            
```

```{code-cell} ipython3
runner = Runner()
runner.train(10)
runner.test()
```
