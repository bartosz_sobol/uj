//
// Created by Bartosz Sobol on 12.11.2021.
//

#ifndef PRIR_PROJ1_RANDOMINITIALDATAGENERATOR_HPP
#define PRIR_PROJ1_RANDOMINITIALDATAGENERATOR_HPP


#include <random>
#include "InitialDataGenerator.h"

class RandomInitialDataGenerator : public InitialDataGenerator {
public:
    RandomInitialDataGenerator(double min, double max, uint64_t seed);

    void fillWithData(double **data, int dataSize, int margin) override;

private:
    uint64_t seed_;
    std::mt19937 random_engine_;
    std::uniform_real_distribution<double> distribution_;

};


#endif //PRIR_PROJ1_RANDOMINITIALDATAGENERATOR_HPP
