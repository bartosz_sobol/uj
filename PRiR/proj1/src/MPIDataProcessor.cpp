//
// Created by Bartosz Sobol on 12.11.2021.
//

#include "mpi.h"

#include "MPIDataProcessor.h"


MPIDataProcessor::MPIDataProcessor() {
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_comm_size_);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_comm_rank_);
}

double **MPIDataProcessor::getResult() {
    return data;
}

void MPIDataProcessor::singleExecution() {
    // return if additional processes not used
    const bool fully_parallelizable = isFullyParallelizable();
    if (!fully_parallelizable and mpi_comm_rank_ != 0) {
        return;
    }

    propagateData();

    // Main task for range of rows
    const auto n_rows = getRowCount(mpi_comm_rank_);
    for (auto i_row = margin; i_row < n_rows - margin; ++i_row) {
        for (auto i_col = margin; i_col < dataSize - margin; ++i_col) {
            auto i_portion = 0;
            for (auto j_row = i_row - margin; j_row <= i_row + margin; ++j_row) {
                for (auto j_col = i_col - margin; j_col <= i_col + margin; ++j_col) {
                    dataPortion[i_portion++] = data[j_row][j_col];
                }
            }
            nextData[i_row][i_col] = function->calc(dataPortion);
        }
    }

    if (mpi_comm_rank_ == 0) {
        for (auto i_row = margin; i_row < n_rows - margin; ++i_row) {
            for (auto i_col = margin; i_col < dataSize - margin; ++i_col) {
                data[i_row][i_col] = nextData[i_row][i_col];
            }
        }
    } else {
        std::swap(data, nextData);
    }

    collectPartialData();
}

void MPIDataProcessor::shareData() {
    //share sizes data between processes and allocate memory
    if (mpi_comm_rank_ == 0) {
        MPI_Bcast(&dataSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dataPortionSize, 1, MPI_INT, 0, MPI_COMM_WORLD);

        const auto n_rows = getRowCount(mpi_comm_rank_);
        nextData = subTableAlloc(n_rows);
    } else {
        MPI_Bcast(&dataSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(&dataPortionSize, 1, MPI_INT, 0, MPI_COMM_WORLD);

        const auto n_rows = getRowCount(mpi_comm_rank_);
        nextData = subTableAlloc(n_rows);
        data = subTableAlloc(n_rows);
        dataPortion = new double[dataPortionSize];
    }
}

void MPIDataProcessor::collectData() {}

void MPIDataProcessor::propagateData() {
    //share data between processes only if needed
    if (isFullyParallelizable()) {
        const auto n_rows = getRowCount(mpi_comm_rank_);
        if (mpi_comm_rank_ == 0) {
            for (auto rank = 1; rank < mpi_comm_size_; ++rank) {
                const auto range = getRowRange(rank);
                for (auto i_row = range.first; i_row < range.second; ++i_row) {
                    MPI_Send(data[i_row], dataSize, MPI_DOUBLE, rank, 0, MPI_COMM_WORLD);
                }
            }
        } else {
            for (auto i_row = 0; i_row < n_rows; ++i_row) {
                MPI_Recv(data[i_row], dataSize, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
        }
    }
}

void MPIDataProcessor::collectPartialData() {
    if (isFullyParallelizable()) {
        if (mpi_comm_rank_ == 0) {
            for (auto rank = 1; rank < mpi_comm_size_; ++rank) {
                const auto range = getRowRange(rank);
                for (auto i_row = range.first + margin; i_row < range.second - margin; ++i_row) {
                    MPI_Recv(data[i_row] + margin, dataSize - 2 * margin, MPI_DOUBLE, rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                }
            }
        } else {
            const auto n_rows = getRowCount(mpi_comm_rank_);
            for (auto i_row = margin; i_row < n_rows - margin; ++i_row) {
                MPI_Send(data[i_row] + margin, dataSize - 2 * margin, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
            }
        }
    }
}

bool MPIDataProcessor::isFullyParallelizable() const noexcept {
    // if number of rows to be processed is less pron comm_size
    return dataSize - 2 * margin >= mpi_comm_size_;
}

std::pair<int, int> MPIDataProcessor::getRowRange(int rank) const noexcept {
    // Get range of rows of initial data to be processed by processed if given rank
    if (isFullyParallelizable()) {
        const int core_size = dataSize - 2 * margin;
        int beg = core_size / mpi_comm_size_ * rank;
        int end = core_size / mpi_comm_size_ * (rank + 1) + 2 * margin;

        const auto remainder = core_size % mpi_comm_size_;
        if (remainder != 0) {
            if (rank < remainder) {
                beg += rank;
                end += rank + 1;
            } else {
                beg += remainder;
                end += remainder;
            }
        }
        return {beg, end};
    }
    // if number of rows to be processed is less pron comm_size just do all work at once (in single process)
    return {0, dataSize};
}

int MPIDataProcessor::getRowCount(int rank) const noexcept {
    const auto row_range = getRowRange(rank);
    return row_range.second - row_range.first;
}

double **MPIDataProcessor::subTableAlloc(int rows) {
    auto **result = new double *[rows];
    for (auto i_row = 0; i_row < rows; ++i_row) {
        result[i_row] = new double[dataSize];
    }
    return result;
}

MPIDataProcessor::~MPIDataProcessor() {
    const auto rows = getRowCount(mpi_comm_rank_);

    if (mpi_comm_rank_ != 0) {
        //in rank 0 I don't know if I'm the owner of data, so I don't delete
        //and in fact data pointer is returned in getResult(), so I cannot delete here
        //swaps between data and nextData doesn't matter here as both arrays have same sizes and are not used anywhere else
        for (auto i_row = 0; i_row < rows; ++i_row) {
            delete[] data[i_row];
        }
        delete[] data;
    }

    for (auto i_row = 0; i_row < rows; ++i_row) {
        delete[] nextData[i_row];
    }
    delete[] nextData;

    delete[] dataPortion;
}


