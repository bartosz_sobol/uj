//
// Created by Bartosz Sobol on 12.11.2021.
//

#ifndef PRIR_PROJ1_MPIDATAPROCESSOR_H
#define PRIR_PROJ1_MPIDATAPROCESSOR_H


#include "DataProcessor.h"


class MPIDataProcessor : public DataProcessor {
public:

    MPIDataProcessor();

    ~MPIDataProcessor() override;

    double **getResult() override;

protected:

    void singleExecution() override;

    void shareData() override;

    void collectData() override;

private:

    void propagateData();

    void collectPartialData();

    bool isFullyParallelizable() const noexcept;

    std::pair<int, int> getRowRange(int rank) const noexcept;

    int getRowCount(int rank) const noexcept;

    double **subTableAlloc(int rows);

    int mpi_comm_size_{};
    int mpi_comm_rank_{};

};


#endif //PRIR_PROJ1_MPIDATAPROCESSOR_H
