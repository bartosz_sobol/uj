#include <iostream>
#include <iomanip>
#include <chrono>
#include "mpi.h"
#include "DataProcessor.h"
#include "MagicFuntion.h"
#include "ArithmeticMeanFunction.h"
#include "SimpleInitialDataGenerator.h"
#include "SequentialDataProcessor.h"
#include "Alloc.h"
#include "RandomInitialDataGenerator.hpp"
#include "MPIDataProcessor.h"

int calcDataPortion(int margin) {
    int dataPortion = margin * 2 + 1;
    dataPortion *= dataPortion;
    return dataPortion;
}

void showTable(double **table, int dataSize) {
    std::cout << "----------------------------------" << std::endl;
    for (int i = 0; i < dataSize; i++) {
        std::cout << std::setw(3) << i << " -> ";
        for (int j = 0; j < dataSize; j++) {
            std::cout << " " << std::showpoint << std::setw(4) << std::setprecision(3)
                      << table[i][j];
        }
        std::cout << std::endl;
    }
}

int main(int argc, char *argv[]) {

    MPI_Init(&argc, &argv);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    const int MARGIN = 2;
    const int DATA_SIZE = 8;
    const int REPETITIONS = 3;

    int dataPortion = calcDataPortion(MARGIN);
    MagicFuntion *mf = new ArithmeticMeanFunction(dataPortion);
            DataProcessor *dp = new SequentialDataProcessor();
//    DataProcessor *dp = new MPIDataProcessor();
    dp->setMagicFunction(mf);

    if (rank == 0) {
        double **initialData = tableAlloc(DATA_SIZE);
        //        InitialDataGenerator *generator = new SimpleInitialDataGenerator(1, 10);
        InitialDataGenerator *generator = new RandomInitialDataGenerator(1, 10, 0);
        generator->fillWithData(initialData, DATA_SIZE, MARGIN);
//        showTable(initialData, DATA_SIZE);

        dp->setInitialData(initialData, DATA_SIZE);
        delete generator;
    }

    const auto start_time = std::chrono::high_resolution_clock::now();
    dp->execute(REPETITIONS);
    const auto end_time = std::chrono::high_resolution_clock::now();

    std::cerr << "EXECUTION_TIME: "<<rank<<' ' << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() << std::endl;
    if (rank == 0) {
        double **result = dp->getResult();
        showTable(result, DATA_SIZE);

        for (int i = 0; i < DATA_SIZE; ++i) {
            delete[] result[i];
        }
        delete[] result;
    }

    delete dp;
    delete mf;


    MPI_Finalize();

    return 0;
}

