//
// Created by Bartosz Sobol on 12.11.2021.
//

#include "RandomInitialDataGenerator.hpp"

RandomInitialDataGenerator::RandomInitialDataGenerator(double min, double max, uint64_t seed) : seed_{seed},
                                                                                                random_engine_{seed_},
                                                                                                distribution_{min, max} {

}

void RandomInitialDataGenerator::fillWithData(double **data, int dataSize, int margin) {
    random_engine_.seed(seed_);
    distribution_.reset();

    for (int i = 0; i < dataSize; ++i) {
        for (int j = 0; j < dataSize; ++j) {
            data[i][j] = distribution_(random_engine_);
        }
    }

}
