import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class SobolPolygonalChainProcessor extends UnicastRemoteObject implements PolygonalChainProcessor {
    protected SobolPolygonalChainProcessor() throws RemoteException {
        super();
    }

    @Override
    public int getConcurrentTasksLimit() throws RemoteException {
        return 8;
    }

    @Override
    public int process(String name, List<Position2D> polygonalChain) throws RemoteException {
        StringBuilder out = new StringBuilder();
        for (Position2D point : polygonalChain) {
            out.append("(");
            out.append(point.getCol());
            out.append(", ");
            out.append(point.getRow());
            out.append(") ");
        }
        System.out.println(out );
        return polygonalChain.get(0).getRow();
    }
}
