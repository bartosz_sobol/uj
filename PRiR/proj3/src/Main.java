import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.newFixedThreadPool;

public class Main {

    static void test(PolygonalChain chain, String name, int offset) throws RemoteException, InterruptedException {
        chain.newPolygonalChain(name, new Position2D(0 + offset, 0 + offset), new Position2D(10 + offset, 10 + offset));
        System.out.format("%s: %d \n", name, chain.getResult(name));

        chain.addLineSegment(name, new Position2D(2 + offset, 2 + offset), new Position2D(3 + offset, 3 + offset));
        System.out.format("%s: %d \n", name, chain.getResult(name));

        chain.addLineSegment(name, new Position2D(6 + offset, 6 + offset), new Position2D(8 + offset, 8 + offset));
        System.out.format("%s: %d \n", name, chain.getResult(name));

        chain.addLineSegment(name, new Position2D(3 + offset, 3 + offset), new Position2D(6 + offset, 6 + offset));
        System.out.format("%s: %d \n", name, chain.getResult(name));

        chain.addLineSegment(name, new Position2D(0 + offset, 0 + offset), new Position2D(2 + offset, 2 + offset));
        System.out.format("%s: %d \n", name, chain.getResult(name));

        chain.addLineSegment(name, new Position2D(8 + offset, 8 + offset), new Position2D(10 + offset, 10 + offset));
        System.out.format("%s: %d \n", name, chain.getResult(name));
        System.out.format("%s: %d %d \n", name, 9, chain.getResult(name));
        System.out.format("%s: %d %d \n", name, 9, chain.getResult(name));
        System.out.format("%s: %d %d \n", name, 9, chain.getResult(name));
        System.out.format("%s: %d %d \n", name, 9, chain.getResult(name));
        System.out.format("%s: %d %d \n", name, 9, chain.getResult(name));
        System.out.format("%s: %d %d \n", name, 9, chain.getResult(name));
        System.out.format("%s: %d %d \n", name, 9, chain.getResult(name));

        TimeUnit.SECONDS.sleep(1);
        System.out.format("%s: %d %d \n", name, 10, chain.getResult(name));

    }

    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(1099);

            java.rmi.Naming.bind("POLYGONAL_PROCESSOR", new SobolPolygonalChainProcessor());
            Start start = new Start();

            PolygonalChain chain = (PolygonalChain) Naming.lookup("POLYGONAL_CHAIN");
            chain.setPolygonalChainProcessorName("POLYGONAL_PROCESSOR");


            ExecutorService job_executors = newFixedThreadPool(10);

            job_executors.execute(() -> {
                try {
                    test(chain, "dupa1", 0);
                } catch (Exception e) {
                }
            });
            job_executors.execute(() -> {
                try {
                    test(chain, "dupa2", 1);
                } catch (Exception e) {
                }
            });
            job_executors.execute(() -> {
                try {
                    test(chain, "dupa3", 2);
                } catch (Exception e) {
                }
            });
            job_executors.execute(() -> {
                try {
                    test(chain, "dupa4", 3);
                } catch (Exception e) {
                }
            });
            job_executors.execute(() -> {
                try {
                    test(chain, "dupa5", 4);
                } catch (Exception e) {
                }
            });
            job_executors.execute(() -> {
                try {
                    test(chain, "dupa6", 5);
                } catch (Exception e) {
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
