//
// Created by Bartosz Sobol on 27.11.2021.
//

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.concurrent.*;

public class Start {

    public Start() throws MalformedURLException, AlreadyBoundException, RemoteException {
        java.rmi.Naming.bind("POLYGONAL_CHAIN", new SobolPolygonalChain());
    }

}


class SobolPolygonalChain extends UnicastRemoteObject implements PolygonalChain {
    public static class Segment {

        public Segment(Position2D begin, Position2D end) {
            this.begin = begin;
            this.end = end;
        }

        Position2D begin;
        Position2D end;

    }

    private class ChainWrapper {

        public ChainWrapper(String name, Position2D begin, Position2D end) {
            this.name = name;
            this.points = new ArrayList<>();
            this.segments_queue = new LinkedList<>();
            this.is_complete = false;
            this.value = null;

            points.add(begin);
            points.add(end);
        }

        public void addSegment(Segment segment) {
            boolean success = addPoints(segment.begin, segment.end);

            if(!success){
                segments_queue.add(segment);
                success = true;
            }

            while (success && !segments_queue.isEmpty()) {
                success = false;
                for (int i_segment = 0; i_segment < segments_queue.size(); ++i_segment) {
                    Segment tmp_segment = segments_queue.get(i_segment);
                    boolean tmp_success = addPoints(tmp_segment.begin, tmp_segment.end);
                    if (tmp_success) {
                        segments_queue.remove(i_segment);
                        --i_segment;
                        success = true;
                    }
                }
            }
        }

        private boolean addPoints(Position2D begin, Position2D end) {
            int i_first = points.indexOf(begin);
            int i_last = points.indexOf(end);

            if (i_first != -1 && i_last != -1) {
                is_complete = true;
            } else if (i_first != -1) {
                points.add(i_first + 1, end);
            } else if (i_last != -1) {
                points.add(i_last, begin);
            } else {
                return false;
            }

            return true;
        }

        public void process_self() {
            try {
                value = chain_processor.process(name, points);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public final String name;
        public final List<Position2D> points;
        public final List<Segment> segments_queue;
        public boolean is_complete;
        public Integer value;

    }

    protected SobolPolygonalChain() throws RemoteException {
        super();
    }

    @Override
    public void newPolygonalChain(String name, Position2D firstPoint, Position2D lastPoint) throws RemoteException {
        chains.put(name, new ChainWrapper(name, firstPoint, lastPoint));
    }

    @Override
    public void addLineSegment(String name, Position2D firstPoint, Position2D lastPoint) throws RemoteException {
        ChainWrapper chain = chains.get(name);
        synchronized (chain) {
            chain.addSegment(new Segment(firstPoint, lastPoint));
            if (chain.is_complete) {
                job_executors.execute(chain::process_self);
            }
        }
    }

    @Override
    public Integer getResult(String name) throws RemoteException {
        ChainWrapper chain = chains.get(name);
        if (chain == null) {
            return null;
        }

        Integer result = chain.value;

        if (result != null) {
            chains.remove(name);
        }
        return result;
    }

    @Override
    public void setPolygonalChainProcessorName(String uri) throws RemoteException {
        try {
            chain_processor = (PolygonalChainProcessor) Naming.lookup(uri);
            job_executors = Executors.newFixedThreadPool(chain_processor.getConcurrentTasksLimit());
        } catch (NotBoundException | MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private PolygonalChainProcessor chain_processor = null;
    private ExecutorService job_executors = null;
    private final Map<String, ChainWrapper> chains = new ConcurrentHashMap<>();

}