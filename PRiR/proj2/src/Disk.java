import java.util.concurrent.atomic.AtomicIntegerArray;

public class Disk implements DiskInterface {

    public Disk(int size) {
        this.size = size;
        this.data = new AtomicIntegerArray(size);
        this.broken = false;
    }

    @Override
    public void write(int sector, int value) throws DiskError {
        if (broken) {
            throw new DiskError();
        } else {
            data.getAndSet(sector, value);
        }
    }

    @Override
    public int read(int sector) throws DiskError {
        if (broken) {
            throw new DiskError();
        } else {
            return data.get(sector);
        }
    }

    @Override
    public int size() {
        return size;
    }

    private final int size;
    public final AtomicIntegerArray data;
    public boolean broken;

}
