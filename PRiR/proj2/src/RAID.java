//
// Created by Bartosz Sobol on 25.11.2021.
//

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.concurrent.Executors.newFixedThreadPool;

class RAID implements RAIDInterface {

    static class DiskSectorPair {

        DiskSectorPair(int disk, int sector) {
            this.disk = disk;
            this.sector = sector;
        }

        int disk;
        int sector;
    }

    public RAID() {
        this.usable_size = 0;

        this.state = RAIDState.UNKNOWN;
        this.broken_disk = -1;

        this.disks = new ArrayList<>();
        this.yield_factor = 100;
        this.wide_lock = new ReentrantLock();
        this.task_executor = newFixedThreadPool(1);
    }

    @Override
    public RAIDState getState() {
        return state;
    }

    @Override
    public void addDisk(DiskInterface disk) {
        if (state == RAIDState.UNKNOWN) {
            disks.add(disk);
            if (disks.size() > 1) {
                usable_size += disk.size();
            }
        }
    }

    @Override
    public void startRAID() {
        if (state == RAIDState.UNKNOWN) {
            state = RAIDState.INITIALIZATION;
            disk_locks = new ReentrantLock[disks.size()];
            for (int i = 0; i < disk_locks.length; ++i) {
                disk_locks[i] = new ReentrantLock();
            }
            task_executor.execute(this::initialize);
        }
    }

    @Override
    public void replaceDisk(DiskInterface disk) {
        if (state == RAIDState.DEGRADED && broken_disk != -1) {
            state = RAIDState.REBUILD;
            disks.set(broken_disk, disk);
            task_executor.execute(this::rebuild);
        }
    }

    @Override
    public void write(int sector, int value) {
        DiskSectorPair physical = getDiskSectorPair(sector);

        wide_lock.lock(); // wide lock because of wide (spare update) access and write
        disk_locks[physical.disk].lock(); // lock disk to force sequential single disk operation
        try {
            if (state == RAIDState.UNKNOWN) {
                return;
            } else if ((state == RAIDState.DEGRADED || state == RAIDState.REBUILD) && broken_disk == physical.disk) {
                int prev = restoreSector(physical.sector);
                updateSpare(physical.sector, value - prev);
            } else {
                int prev = disks.get(physical.disk).read(physical.sector);
                disks.get(physical.disk).write(physical.sector, value);
                updateSpare(physical.sector, value - prev);
            }
        } catch (DiskInterface.DiskError error) {
            state = RAIDState.DEGRADED;
            broken_disk = physical.disk;
            int prev = restoreSector(physical.sector);
            updateSpare(physical.sector, value - prev);
            //System.out.format("DiskException in write sec %d\n", physical.sector);
        } finally {
            disk_locks[physical.disk].unlock();
            wide_lock.unlock();
        }
    }

    @Override
    public int read(int sector) {
        DiskSectorPair physical = getDiskSectorPair(sector);
        disk_locks[physical.disk].lock(); // lock disk to force sequential single disk operation
        try {
            if (state == RAIDState.UNKNOWN) {
                return 0;
            } else if ((state == RAIDState.DEGRADED || state == RAIDState.REBUILD) && broken_disk == physical.disk) {
                return restoreSector(physical.sector);
            }
            return disks.get(physical.disk).read(physical.sector);
        } catch (DiskInterface.DiskError error) {
            state = RAIDState.DEGRADED;
            broken_disk = physical.disk;
            //System.out.format("DiskException in read sec %d\n", physical.sector);
            return restoreSector(physical.sector);
        } finally {
            disk_locks[physical.disk].unlock();
        }
    }

    @Override
    public int size() {
        return usable_size;
    }

    @Override
    public void shutdown() {
        task_executor.shutdown();
        try {
            if (!task_executor.awaitTermination(1, TimeUnit.SECONDS)) {
                task_executor.shutdownNow();
            }
        } catch (InterruptedException ignored) {
            //System.out.println("Exception in shutdown");
            task_executor.shutdownNow();
        } finally {
            state = RAIDState.UNKNOWN;
        }
    }

    private void initialize() {
        try {
            for (int i_sector = 0; i_sector < disks.get(0).size(); ++i_sector) {
                wide_lock.lock(); // wide lock because of wide access and write
                buildSpareSector(i_sector);
                wide_lock.unlock();
                if (i_sector % yield_factor == 0) {
                    Thread.yield();
                    //TimeUnit.MILLISECONDS.sleep(100);
                    //Thread.yield();
                }
            }
            state = RAIDState.NORMAL;
        } catch (DiskInterface.DiskError /*| InterruptedException*/ ignored) {
            //System.out.println("DiskException in init. SHOULD NOT HAPPEN");
        } finally {
            if (wide_lock.isLocked()) {
                wide_lock.unlock();
            }
        }
    }

    private void rebuild() {
        try {
            for (int i_sector = 0; i_sector < disks.get(0).size(); ++i_sector) {
                wide_lock.lock();  // wide lock because of wide access and write
                if (broken_disk == disks.size() - 1) {
                    buildSpareSector(i_sector);
                } else {
                    disks.get(broken_disk).write(i_sector, restoreSector(i_sector));
                }
                wide_lock.unlock();
                if (i_sector % yield_factor == 0) {
                    Thread.yield();
                    //TimeUnit.MILLISECONDS.sleep(100);
                    //Thread.yield();
                }
            }
            synchronized (this) {
                broken_disk = -1;
                state = RAIDState.NORMAL;
            }
        } catch (DiskInterface.DiskError /*| InterruptedException*/ ignored) {
            //System.out.println("DiskException in rebuild. SHOULD NOT HAPPEN");
        } finally {
            if (wide_lock.isLocked()) {
                wide_lock.unlock();
            }
        }
    }

    private int restoreSector(int sector) {
        try {
            int partial_sum = 0;
            for (int i_disk = 0; i_disk < disks.size() - 1; ++i_disk) {
                if (i_disk != broken_disk) {
                    disk_locks[i_disk].lock();
                    partial_sum += disks.get(i_disk).read(sector);
                    disk_locks[i_disk].unlock();
                }
            }
            disk_locks[disks.size()-1].lock();
            return getSpareDisk().read(sector) - partial_sum;
        } catch (DiskInterface.DiskError ignored) {
            //System.out.format("DiskException in sector restore sec %d. SHOULD NOT HAPPEN\n", sector);
            return 0;
        }finally{
            disk_locks[disks.size()-1].unlock();
        }
    }

    private void buildSpareSector(int sector) throws DiskInterface.DiskError {
        int spare_value = 0;
        for (int i_disk = 0; i_disk < disks.size() - 1; ++i_disk) {
            spare_value += disks.get(i_disk).read(sector);
        }
        getSpareDisk().write(sector, spare_value);
    }

    private void updateSpare(int sector, int diff) {
        try {
            if (!(state == RAIDState.DEGRADED && broken_disk == disks.size() - 1)) {
                int prev = getSpareDisk().read(sector);
                getSpareDisk().write(sector, prev + diff);
            }
        } catch (DiskInterface.DiskError ignored) {
            state = RAIDState.DEGRADED;
            broken_disk = disks.size() - 1;
            //System.out.println("DiskException in spare update");
        }
    }

    private DiskInterface getSpareDisk() {
        return disks.get(disks.size() - 1);
    }

    private DiskSectorPair getDiskSectorPair(int sector) {
        return new DiskSectorPair(sector / (disks.get(0).size()), sector % disks.get(0).size());
    }


    private int usable_size;
    private ReentrantLock[] disk_locks;

    private volatile RAIDState state;
    private volatile int broken_disk;

    private final ArrayList<DiskInterface> disks;
    private final int yield_factor;
    private final ReentrantLock wide_lock;
    private final ExecutorService task_executor;

}
