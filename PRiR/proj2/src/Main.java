import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.newFixedThreadPool;

public class Main {

    public static void main(String[] args) {
        try {
            ExecutorService executor = newFixedThreadPool(10);

            RAID raid = new RAID();

            int size = 128;

            Disk disk_1 = new Disk(size);
            Disk disk_2 = new Disk(size);
            Disk disk_3 = new Disk(size);
            Disk disk_new = new Disk(size);

            for (int i = 0; i < size; ++i) {
                disk_1.data.set(i, i);
                disk_2.data.set(i, i + 3);
                disk_3.data.set(i, -2 * i);
                disk_new.data.set(i, 5000);
            }

            raid.addDisk(disk_1);
            raid.addDisk(disk_2);
            raid.addDisk(disk_3);

            System.out.println(raid.getState()); //unk
            raid.startRAID();
            System.out.println(raid.getState()); //init
            System.out.println(raid.size());
//            TimeUnit.SECONDS.sleep(2);
            System.out.println(raid.getState()); //normal
            TimeUnit.SECONDS.sleep(2);
            disk_3.broken = true;


            executor.execute(() -> raid.write(10, 2));

            executor.execute(() -> raid.write(50, 4));
            executor.execute(() -> raid.write(90, 8));
            executor.execute(() -> raid.write(130, 16));
            executor.execute(() -> raid.write(170, 32));
            executor.execute(() -> raid.write(210, 64));
            executor.execute(() -> raid.write(250, 128));
            System.out.println(raid.getState()); //normal
            TimeUnit.SECONDS.sleep(2);
            raid.replaceDisk(disk_new);

            executor.execute(() -> {
                int sector = 10;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 50;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 90;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 130;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 170;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 210;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 250;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });

            while(raid.getState()!= RAIDInterface.RAIDState.NORMAL){}
            System.out.println(raid.getState());
        disk_1.broken=true;

            System.out.println("NEW");
            System.out.println(raid.getState());
//            TimeUnit.SECONDS.sleep(2);
//            System.out.println(raid.getState());

            executor.execute(() -> {
                int sector = 10;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 50;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 90;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 130;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 170;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 210;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 250;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });


            System.out.println(raid.getState()); //normal
            TimeUnit.SECONDS.sleep(2);
            System.out.println(raid.getState());

            executor.execute(() -> {
                int sector = 9;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 50;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 90;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 130;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 170;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 210;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });
            executor.execute(() -> {
                int sector = 250;
                int val = raid.read(sector);
                System.out.format("%d %d\n", sector, val);
            });


            raid.shutdown();
            System.out.println(raid.getState()); //unk

            executor.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
