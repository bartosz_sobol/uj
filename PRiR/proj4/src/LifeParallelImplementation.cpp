//
// Created by Bartosz Sobol on 07.12.2021.
//

#include <utility>
#include <omp.h>

#include "LifeParallelImplementation.h"


drand48_data LifeParallelImplementation::rand_data;

LifeParallelImplementation::LifeParallelImplementation() {
#pragma omp parallel default(none) num_threads(8)
    {
        srand48_r(omp_get_thread_num(), &rand_data);
    }
}

// do zrownoleglenia
void LifeParallelImplementation::oneStep() {
#pragma omp parallel for collapse(2) default(none)
    for (int i_row = 0; i_row < size; ++i_row) {
        for (int i_col = 0; i_col < size; ++i_col) {
            const int neighbours = liveNeighbours(i_row, i_col);

            double random_tmp;
            drand48_r(&rand_data, &random_tmp);

            if (cells[i_row][i_col] != 0) {
                if (rules->cellDies(neighbours, age[i_row][i_col], random_tmp)) {
                    nextGeneration[i_row][i_col] = 0;
                    age[i_row][i_col] = 0;
                } else {
                    nextGeneration[i_row][i_col] = 1;
                    ++age[i_row][i_col];
                }
            } else {
                if (rules->cellBecomesLive(neighbours, neighboursAgeSum(i_row, i_col), random_tmp)) {
                    nextGeneration[i_row][i_col] = 1;
                    age[i_row][i_col] = 0;
                } else {
                    nextGeneration[i_row][i_col] = 0;
                }
            }
        }
    }

    std::swap(nextGeneration, cells);
}

// do zrownoleglenia
double LifeParallelImplementation::avgNumerOfLiveNeighboursOfLiveCell() {
    int sumOfNeighbours = 0;
    int counter = 0;

#pragma omp parallel for collapse(2) default(none) reduction(+ : sumOfNeighbours) reduction(+ : counter)
    for (int i_row = 1; i_row < size - 1; ++i_row) {
        for (int i_col = 1; i_col < size - 1; ++i_col) {
            if (cells[i_row][i_col] != 0) {
                int sum_tmp = liveNeighbours(i_row, i_col);
                sumOfNeighbours += sum_tmp;
                ++counter;
            }
        }
    }

    return counter == 0 ? 0. : (double) sumOfNeighbours / (double) counter;
}

// do zrownoleglenia
int LifeParallelImplementation::maxSumOfNeighboursAge() {
    int max = 0;

#pragma omp parallel for collapse(2) default(none) reduction(max : max)
    for (int i_row = 1; i_row < size - 1; ++i_row) {
        for (int i_col = 1; i_col < size - 1; ++i_col) {
            const int sumOfNeighboursAge = neighboursAgeSum(i_row, i_col);
            if (max < sumOfNeighboursAge) {
                max = sumOfNeighboursAge;
            }
        }
    }

    return max;
}

// do zrownoleglenia
int *LifeParallelImplementation::numberOfNeighboursStatistics() {
    constexpr int N_NEIGHBOURS = 9;
    int *tbl = new int[N_NEIGHBOURS];
    for (int i = 0; i < N_NEIGHBOURS; ++i) {
        tbl[i] = 0;
    }

#pragma omp parallel default(none) shared(tbl)
    {
        int tbl_tmp[N_NEIGHBOURS] = {0};

#pragma omp for collapse(2)
        for (int i_row = 1; i_row < size - 1; ++i_row) {
            for (int i_col = 1; i_col < size - 1; ++i_col) {
                const int live_neighbours = liveNeighbours(i_row, i_col);
                ++tbl_tmp[live_neighbours];
            }
        }

#pragma omp critical
        {
            for (int i_neighbour = 0; i_neighbour < N_NEIGHBOURS; ++i_neighbour) {
                tbl[i_neighbour] += tbl_tmp[i_neighbour];
            }
        }

    }

    return tbl;
}
