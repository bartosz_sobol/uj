#include<iostream>
#include <unistd.h>
#include <chrono>

#include"Life.h"
#include"Rules.h"
#include"RandomRules.h"
#include"LifeSequentialImplementation.h"
#include"LifeParallelImplementation.h"


constexpr int SIZE = 20;
constexpr int RUNS = 11;

void showTable(int **cells, int size) {
    for (int i_row = 0; i_row < size; ++i_row) {
        for (int i_col = 0; i_col < size; ++i_col) {
            std::cout << (cells[i_row][i_col] ? "X " : ". ");
        }
        std::cout << std::endl;
    }
}

void showVector(int *tbl, int size) {
    for (int i = 0; i < size; ++i) {
        std::cout << tbl[i] << ", ";
    }
    std::cout << std::endl;
}

void glider(Life *l, int col, int row) {
    l->setLiveCell(col, row);
    l->setLiveCell(col + 1, row);
    l->setLiveCell(col + 2, row);
    l->setLiveCell(col, row + 1);
    l->setLiveCell(col + 1, row + 2);
}

int main(int argc, char **argv) {

//        Life *l = new LifeSequentialImplementation(); // dla porownania
    Life *l = new LifeParallelImplementation();
    Rules *r = new RandomRules();

    l->setRules(r);
    l->setSize(SIZE);

    glider(l, 5, 5);
    glider(l, 10, 5);
    glider(l, 10, 10);
    glider(l, 5, 10);

    int *stat;

    int run_counter = 0;
    const auto start_time = std::chrono::high_resolution_clock::now();
    while (++run_counter < RUNS) {
        l->oneStep();
        //                showTable(l->getCurrentState(), SIZE);
        std::cout << l->avgNumerOfLiveNeighboursOfLiveCell() << std::endl;
        std::cout << l->maxSumOfNeighboursAge() << std::endl;
        stat = l->numberOfNeighboursStatistics();
        showVector(stat, 9);
        delete[] stat;
        //                usleep(250000);
    }
    const auto end_time = std::chrono::high_resolution_clock::now();
    std::cerr << "EXECUTION_TIME: " << ' ' << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() << std::endl;
    showTable(l->getCurrentState(), SIZE);

    delete r;
    delete l;

    return 0;
}

