//
// Created by Bartosz Sobol on 07.12.2021.
//

#ifndef PRIR_PROJ4_LIFEPARALLELIMPLEMENTATION_H
#define PRIR_PROJ4_LIFEPARALLELIMPLEMENTATION_H

#include <cstdlib>
#include"Life.h"


class LifeParallelImplementation : public Life {
public:

    LifeParallelImplementation();

    double avgNumerOfLiveNeighboursOfLiveCell() override;

    int maxSumOfNeighboursAge() override;

    int *numberOfNeighboursStatistics() override;

    void oneStep() override;

private:

    static drand48_data rand_data;
#pragma omp threadprivate(rand_data)

};

#endif //PRIR_PROJ4_LIFEPARALLELIMPLEMENTATION_H
